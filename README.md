# README #

OS/2 port of wpa_supplicant from Jouni Malinen. See README.

### What is this repository for? ###

* Source code for OS/2 port until it makes it into official repository
* Starting with version 2.7-devel from http://w1.fi/wpa_supplicant/ at 20180310
* See here for [official repository](http://w1.fi/wpa_supplicant/)
* See here for [previous OS/2 repository (v2.2)](http://trac.netlabs.org/ports/browser/wpa_supplicant/)

This is only a temporary repository. Work in progress.

